const webpack = require('webpack');
const webpackMerge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const commonConfig = require('./webpack.common.js')
const helpers = require('./helpers')

module.exports = webpackMerge(commonConfig, {
    output: {
        path: helpers.root('dist'),
        filename: './[name].bundle.js',
        chunkFilename: './[id].chunk.js',
        crossOriginLoading: false
    },
    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});