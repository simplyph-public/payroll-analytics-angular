import { Component } from '@angular/core'

@Component({
    selector: 'pa-at',
    template: `Hello from {{title}} <router-outlet></router-outlet>`
})
export class ErrorComponent {
    title = "Error Component";
}