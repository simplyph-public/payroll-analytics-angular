import { NgModule } from '@angular/core'

import { ErrorRoutingModule } from './error-routing.module'

import { ErrorComponent } from './error.component'
import { PageNotFoundComponent } from './page-not-found.component'

@NgModule({
    declarations: [
        ErrorComponent,
        PageNotFoundComponent
    ],
    imports: [
        ErrorRoutingModule
    ]    
})
export class ErrorModule { }