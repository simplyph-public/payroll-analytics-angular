import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ErrorComponent } from './error.component'
import { PageNotFoundComponent } from './page-not-found.component'

const routes: Routes = [
    {        
        path: '',
        component: ErrorComponent,
        children: [
            { path: '', component: PageNotFoundComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ErrorRoutingModule { }