import { Component } from '@angular/core'

@Component({
    selector: 'pa-error-not-found',
    template: `Page not found!`
})
export class PageNotFoundComponent { }