import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AttendanceComponent } from './attendance.component'
import { CompileComponent } from './compile/compile.component'

const routes: Routes = [
    {
        path: '',
        component: AttendanceComponent,
        children: [
            { path: '', component: AttendanceComponent },            
            { path: 'compile', component: CompileComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AttendanceRoutingModule { }