import { NgModule } from '@angular/core'

import { AttendanceRoutingModule } from './attendance-routing.module'

import { AttendanceComponent } from './attendance.component'
import { CompileComponent } from './compile/compile.component'

@NgModule({
    declarations: [
        AttendanceComponent,
        CompileComponent
    ],
    imports: [
        AttendanceRoutingModule
    ]    
})
export class AttendanceModule { }