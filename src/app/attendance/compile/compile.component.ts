import { Component } from '@angular/core'

@Component({
    selector: 'pa-at-compile',
    template: `Hello from {{title}}`
})
export class CompileComponent {
    title = "Compile Component";
}