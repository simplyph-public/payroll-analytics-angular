import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { MaterialModule } from '../material/material.module';

import { CoreRoutingModule } from './core-routing.module'
import { GraphQLModule } from '../graphql/graphql.module';
import { LoginComponent } from './login/login.component';
import { 
    SidebarComponent,
    ToolbarComponent,
    FooterComponent,
    HomeComponent
} from './home/home.component';
import { AuthService } from './services/auth.service';
import { CoreService } from './services/core.service';

import { CORE_CONFIG, CORE_DI_CONFIG } from './core.config';
import { AuthGuard } from './services/auth.guard';
import { TokenInterceptor } from './services/token.interceptor';

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        SidebarComponent,
        ToolbarComponent,
        FooterComponent
    ],
    imports: [
        CommonModule,
        CoreRoutingModule,
        MaterialModule,
        GraphQLModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    exports: [
        RouterModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: CORE_CONFIG,
            useValue: CORE_DI_CONFIG
        },
        AuthService,
        CoreService,
        AuthGuard
    ]
})
export class CoreModule { }