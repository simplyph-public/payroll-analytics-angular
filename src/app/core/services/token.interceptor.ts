import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { Router } from '@angular/router'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/do'

import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private router: Router, private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        if (req.headers.get('No-Auth') == "True") {
            return next.handle(req.clone());
        }

        let authReq = req.clone({
            setHeaders: {                
                'Authorization': `Bearer ${this.authService.getAccessToken()}`
            }
        });

        return next.handle(authReq).do(
            data => { }, 
            err => {
                if (err.status === 401)
                    this.router.navigate(['/login']);
                else (err.status === 403)
                    this.authService.renewAccessToken();
                }
            );
    }
}