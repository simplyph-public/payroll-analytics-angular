import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CoreConfig, CORE_CONFIG } from '../core.config';

@Injectable()
export class CoreService {

    constructor(
        private httpClient: HttpClient,
        @Inject(CORE_CONFIG) private config: CoreConfig) { }

    getSampleData() {
        let url = this.config.baseUri + "/auth/baf";

        return this.httpClient.get<any>(url);
    }

}