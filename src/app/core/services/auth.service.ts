import { Component, Injectable, Inject } from '@angular/core'
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'

import { AuthRefreshToken, AuthAccessToken } from '../models/auth.model';
import { CORE_CONFIG, CoreConfig } from '../core.config';
import { Router } from '@angular/router';

let NoAuthHeader = new HttpHeaders({
    'No-Auth': 'True'
});

@Injectable()
export class AuthService {

    private accessToken: string;
    private maxRetry = 3;
    private retries = 1;

    constructor(
        private router: Router,
        private httpClient: HttpClient,
        @Inject(CORE_CONFIG) private config: CoreConfig
    ) { }

    login(username: string, password: string) {        
        return this.authenticate(username, password).map(data => {
            if(data.access_token) {
                this.saveAccessToken(data.access_token);
            }

            if(data.refresh_token) {                
                this.setToken(data.refresh_token);
            }

            return data;
        });
    }

    logout() {
        this.setToken(null);
        this.redirectToLogin(null);
    }

    private setToken(refreshToken: string) {
        if(refreshToken) {
            localStorage.setItem(this.config.oauthKey, refreshToken);
        } else {
            localStorage.removeItem(this.config.oauthKey);
        }        
    }

    getToken() {
        return localStorage.getItem(this.config.oauthKey);
    }

    authenticate(username: string, password: string) {
        let body = new HttpParams()
            .set('grant_type', 'password')
            .set('client_id', 'Admin')
            .set('client_secret', 'swakAdmin_webApi_2017')
            .set('username', username)
            .set('password', password);

        return this.httpClient.post<AuthRefreshToken>(this.config.apiTokenEndpoint, body, { headers: NoAuthHeader });
    }
    
    renewAccessToken() {
        let body = new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', 'Admin')
            .set('client_secret', 'swakAdmin_webApi_2017')
            .set('refresh_token', this.getToken());

        this.httpClient.post<AuthAccessToken>(this.config.apiTokenEndpoint, body, { headers: NoAuthHeader }).subscribe(
            data => this.accessToken = data.access_token,
            err => {
                if(this.retries < this.maxRetry) {
                    this.retries++;
                    this.renewAccessToken();
                } else {
                    console.log("Cannot authenticate the current user.");
                }
            }
        );
    }

    isAuthenticated() {
        if(this.getToken()) {
            return true;
        } else {
            return false;
        }
    }

    // Access Tokens
    private saveAccessToken(accessToken: string) {
        if(accessToken) {
            this.accessToken = accessToken;
        }
    }

    getAccessToken(): string {
        if(this.accessToken)
            return this.accessToken;
        else
            this.renewAccessToken();        
    }

    redirectToLogin(returnUrl: string) {
        if(returnUrl) {
            this.router.navigate(['/login'], { queryParams: { returnUrl: returnUrl } });
        } else {
            this.router.navigate(['/login']);
        }        
    }
}