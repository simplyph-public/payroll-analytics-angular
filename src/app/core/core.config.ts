import { InjectionToken } from '@angular/core';

import { CoreConfig } from './core-config';
export { CoreConfig } from './core-config';

export const CORE_CONFIG = new InjectionToken<CoreConfig>('core.config');

export const CORE_DI_CONFIG: CoreConfig = {
    appTitle: 'Payroll Analytics',
    baseUri: 'http://mdrpayrollapi.swak.net.ph',
    apiTokenEndpoint: 'http://mdrpayrollapi.swak.net.ph/token',
    apiGraphQLEndpoint: 'http://mdrpayrollapi.swak.net.ph/graphql',
    oauthKey: 'oauth'
}