import { Component, OnInit } from '@angular/core'
import { AuthService } from '../services/auth.service';
import { CoreService } from '../services/core.service';

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { EMPLOYEES_QUERY } from '../../graphql/graphql';

export { SidebarComponent } from './sidebar.component';
export { ToolbarComponent } from './toolbar.component';
export { FooterComponent } from './footer.component';

@Component({
    selector: 'pa-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    
    isDrawerAutoResize: boolean;

    constructor(private apollo: Apollo, private authService: AuthService, private coreService: CoreService) { }

    ngOnInit() {
        this.isDrawerAutoResize = false;
    }

    generateToken() {
        this.authService.login("knowell", "knowell10").subscribe(data => { console.log(data) });
    }

    getSampleData() {
        this.coreService.getSampleData().subscribe(data => { console.log(data) }, error => { console.log(error) });
    }

    setAutoResize(isAutoResize: boolean) {
        this.isDrawerAutoResize = isAutoResize;
    }

    getGraphQL() {
        this.apollo.query({
            query: EMPLOYEES_QUERY
        }).subscribe(console.log);
    }
}