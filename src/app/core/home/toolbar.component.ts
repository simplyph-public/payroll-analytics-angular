import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { CORE_CONFIG, CoreConfig } from '../core.config'

@Component({
    selector: 'pa-toolbar',
    templateUrl: './toolbar.component.html'
})
export class ToolbarComponent implements OnInit {
    
    private toolbarTitle: string;
    private showSearch: boolean;
    private searchControl = new FormControl();
    private options = ['Knowell', 'Renz', 'Jayson'];

    private filteredOptions: Observable<string[]>;

    constructor(@Inject(CORE_CONFIG) private config: CoreConfig) {

    }

    ngOnInit() {
        this.showSearch = false;
        this.toolbarTitle = this.config.appTitle;
        this.filteredOptions = this.searchControl.valueChanges
            .pipe(
                startWith(''),
                map(val => this.filter(val))
            )
    }

    toggleSearch() {
        this.showSearch = !this.showSearch;
    }

    filter(val: string): string[] {
        return this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) >= 0);
    }
}