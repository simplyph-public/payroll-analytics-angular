import { Component, ElementRef, HostListener, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
    selector: 'pa-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

    @Output() onMiniOfDrawer = new EventEmitter();

    isMiniEnabled: boolean;
    isDrawerOpen: boolean;

    constructor(private el: ElementRef) { }

    ngOnInit() {
        this.isMiniEnabled = false;
        this.isDrawerOpen = true;
    }

    @HostListener('mouseenter') onHoverInSidenav() {
        if(this.isMiniEnabled)
            this.toggleDrawer();
    }

    @HostListener('mouseleave') onLeaveInSidenav() {
        if(this.isMiniEnabled)
            this.toggleDrawer();
    }

    toggleDrawer() {
        this.isDrawerOpen = !this.isDrawerOpen;
    }

    toggleMiniSidenav() {
        this.isMiniEnabled = !this.isMiniEnabled;
        if(this.isMiniEnabled) {
            this.toggleDrawer();
        }
        this.onMiniOfDrawer.emit(this.isMiniEnabled);
    }
    
}