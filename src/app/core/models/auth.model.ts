export class AuthRefreshToken {
    access_token: string;
    expires_in: number;
    refresh_token: string;
    userName: string;
}

export class AuthAccessToken {
    access_token: string;
    expires_in: number;
    userName: string;
}

export class RefreshTokenParam {
    grant_type: string;
    refresh_token: string;
    client_id: string;
    client_secret: string;
}