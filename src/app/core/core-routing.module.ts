import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { LoginComponent } from './login/login.component'
import { HomeComponent } from './home/home.component'
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: HomeComponent
    },
    {
        path: 'home',
        pathMatch: 'full',
        redirectTo: ''
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'attendance',
        canActivate: [AuthGuard],
        component: HomeComponent,
        loadChildren: '../attendance/attendance.module#AttendanceModule'
    },
    {
        path: 'payroll',
        canActivate: [AuthGuard],
        component: HomeComponent,
        loadChildren: '../payroll/payroll.module#PayrollModule'
    },
    {
        path: '**',
        component: HomeComponent,
        loadChildren: '../error/error.module#ErrorModule'
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class CoreRoutingModule { }