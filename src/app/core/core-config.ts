export interface CoreConfig {
    baseUri: string;
    apiTokenEndpoint: string;
    apiGraphQLEndpoint: string;
    appTitle: string;
    oauthKey: string;
}