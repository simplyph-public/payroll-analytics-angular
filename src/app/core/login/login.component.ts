import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'pa-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    title = "Sign in";
    subtitle = "to continue to Payroll Analytics Angular App";

    loginForm: FormGroup;
    loginError = false;
    loginErrorMessage: string = null;
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private authService: AuthService
    ) {
        this.createLoginForm();
    }

    ngOnInit() {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    createLoginForm() {
        this.loginForm = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })
    }

    tryLogin(e: any) {
        e.preventDefault();

        this.authService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
            data => {
                console.log(data);
                this.router.navigate(['/home']);
            }, error => {
                console.log(error);
            }
        );
    }

    get username() {
        return this.loginForm.get('username');
    }

    get password() {
        return this.loginForm.get('password');
    }
}