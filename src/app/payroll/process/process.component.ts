import { Component } from '@angular/core'

@Component({
    selector: 'pa-py-process',
    template: `Hello from {{title}}`
})
export class ProcessComponent {
    title = "Process Component";
}