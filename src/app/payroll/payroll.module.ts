import { NgModule } from '@angular/core'

import { PayrollRoutingModule } from './payroll-routing.module'

import { PayrollComponent } from './payroll.component'
import { ProcessComponent } from './process/process.component'

@NgModule({
    declarations: [
        PayrollComponent,
        ProcessComponent
    ],
    imports: [
        PayrollRoutingModule
    ]    
})
export class PayrollModule { }