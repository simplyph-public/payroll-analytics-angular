import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PayrollComponent } from './payroll.component'
import { ProcessComponent } from './process/process.component'

const routes: Routes = [
    {
        path: '',
        component: PayrollComponent,
        pathMatch: 'full'
    },
    {
        path: 'process',
        component: PayrollComponent,
        children: [
            { path: '', component: ProcessComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PayrollRoutingModule { }