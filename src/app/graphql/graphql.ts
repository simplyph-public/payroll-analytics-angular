import gql from 'graphql-tag';

export const EMPLOYEES_QUERY = gql`
    query EmployeesQuery {
        employees {
            items {
                firstName
                lastName
            }
        }
    }
`