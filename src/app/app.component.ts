import { Component } from "@angular/core";

import { Spinkit } from 'ng-http-loader/spinkits';

@Component({
    selector: 'pa-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    public spinkit = Spinkit;
    title = "App Component";
}